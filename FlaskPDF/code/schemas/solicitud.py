from marshmallow import Schema, fields


class SolicitudSchema(Schema):
    folio = fields.Str(required=True)
    nombrePaciente = fields.Str(required=True)
    apellidoPaciente = fields.Str(required=True)
    nacDia = fields.Int(required=True)
    nacMes = fields.Int(required=True)
    nacAnio = fields.Int(required=True)
    genero = fields.Str(required=True)

    categoriaElegida = fields.Str(required=True)
    estudioElegido = fields.Str(required=True)
    metodologiaEstudio = fields.Str(required=True)
    claveEstudio = fields.Str(required=True)

    nomDoc = fields.Str(required=True)
    cedula = fields.Str(required=True)
    email = fields.Str(required=True)
    telefono = fields.Str(required=True)

    primarioSelect = fields.Str(required=True)
    metastasicoSelect = fields.Str()

    muestraBloque = fields.Boolean(required=True)
    muestraLaminillas = fields.Boolean(required=True)
    numeroMuestraBloque = fields.Str()
    numeroMuestraLaminillas = fields.Str()
    
    muestraMes = fields.Int(required=True)
    muestraAnio = fields.Int(required=True)
    diagnostico = fields.Str(required=True)
    tratamientosPrevios = fields.Str()
    labRadio = fields.Str(required=True)

    inputNombreHospital = fields.Str(required=True)
    inputCalleNumero = fields.Str(required=True)
    inputCodigoPostal = fields.Str(required=True)
    inputColinia = fields.Str(required=True)
    inputAlcaldiaMun = fields.Str(required=True)
    inputEstado = fields.Str(required=True)
    inputReferencias = fields.Str(required=True)
    horarioRecoger = fields.Str(required=True)

    costoEstudio = fields.Int()
    firmaPaciente = fields.Str()
    firmaMedico = fields.Str()

    docHistopato = fields.Str()
    docOtros = fields.Str()


class ListoOFirmaSchema(Schema):
    folio = fields.Str(required=True)
    nomDoc = fields.Str(required=True)
    correoDoc = fields.Str(required=True)
