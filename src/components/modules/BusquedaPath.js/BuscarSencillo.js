import React from 'react';

const BuscarSencillo = ({ listado, setParam }) => {
  return (
    <div className="card-body">
      <ul className="list-group">
        <form className="mb-3">
          <div className="form-row">
            <div className="col-12">
              <input className="form-control mr-sm-2" type="search" placeholder="Buscar por nombre" aria-label="Buscar"
                onChange={e => setParam(e.target.value)}
                value={nomDoc}
              />
            </div>
          </div>
        </form>

        <ul className="list-group">
          {
            doctoresFiltrados.map(doctor => {
              return <Link key={doctor.cedula} to={`/${DetailDoctor}/${doctor.cedula}`} className="list-group-item list-group-item-action">Doctor {doctor.nom}</Link>;
            })
          }
        </ul>
      </ul>
    </div>
  );
};

export default BuscarSencillo;