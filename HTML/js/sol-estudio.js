'use strict'

const buscarMedico = document.getElementById('buscarMedico');
const seleccionEspecialidad = document.getElementById('seleccionEspecialidad');
const infoPaciente = document.getElementById('infoPaciente');
const ap_ci = document.getElementById('ap_ci');
const seccionDePago = document.getElementById('seccionDePago');

const apartadoSolcitud = document.getElementsByClassName('apartadoSolcitud');
const btnsCrumb = document.getElementsByClassName('btn-crumb');

const cambioApartadoSolicitud = (btn, seccion) => {
  switch (seccion) {
    case 1:
      ocultarApartados();
      buscarMedico.style.display = 'block';
      break;
    case 2:
      ocultarApartados();
      seleccionEspecialidad.style.display = 'block';
      break;
    case 3:
      ocultarApartados();
      infoPaciente.style.display = 'block';
      break;
    case 4:
      ocultarApartados();
      ap_ci.style.display = 'block';
      break;
    case 5:
      ocultarApartados();
      seccionDePago.style.display = 'block';
      break;
    default:
      ocultarApartados();
  }
  btn.classList.add('breadcrumb-item2');
  btn.classList.remove('breadcrumb-item1');
}

const ocultarApartados = () => {
  for (let apartado of apartadoSolcitud) {
    apartado.style.display = 'none';
  }

  for (let btnCrumb of btnsCrumb) {
    btnCrumb.classList.remove('breadcrumb-item2');
    btnCrumb.classList.add('breadcrumb-item1');
  }
}
