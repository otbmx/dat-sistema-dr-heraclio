import base64
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail, Attachment, FileContent, FileName, FileType, Disposition

correo_base = 'contacto@diagnostiqueatiempo.com'


def envioSendgrid(message):
    try:
        sg = SendGridAPIClient(
            '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'
        )
        response = sg.send(message)
        return True
    except Exception as e:
        print('Error al enviar correo', e)
        return False


def sendEmailSolicitud(folio, nomDoc, correoDoc, docHistopato='', docOtros=''):
    ''' Args: correoDoc, folio, nomDoc, docHistopato, docOtros '''

    message = Mail(
        from_email=(correo_base, 'Equipo de Diagnostique A Tiempo'),
        to_emails=[correo_base, correoDoc],
        # Programa Diagnóstico NTRK – Bayer. 
        subject=f'Solicitud #{folio} recibida',
        html_content=f'''
        <p>Estimad@ Dr@ {nomDoc},</p>
        <p>Su solicitud de estudio con folio #{folio} ha sido recibida con éxito.</p>
        <p>Puede seguir el progreso de la misma en el siguiente <a href="https://diagnostiqueatiempo.com/" target= "_blank">enlace</a></p>
        <p>Si necesita soporte adicional, puede contactarnos por las siguientes vías:</p>
        <ul>
        <li>Teléfono: +52 55 8880 7871</li>
        <li>Correo electrónico: respondiendo este correo electrónico o en contacto@diagnostiqueatiempo.com si su consulta es sobre otro tópico.</li>
        <li>Whatsapp: en el siguiente <a href="https://wa.me/525588807871">enlace</a></li>
        <br>
        <p>Atentamente,</p>
        <p>El equipo de Diagnostique A Tiempo</p>
        <br>
        <p>www.diagnostiqueatiempo.com</p>
        <small>Si no ha solicitado ningún estudio nuevo, favor de ponerse en contacto con el equipo.</small>
        <br>
        <small>De acuerdo a la Ley General de Protección de Datos en Posesión de Particulares, puede consultar nuestro Aviso de Privacidad y Términos y Condiciones del Servicio en el siguiente <a href="https://diagnostiqueatiempo.com/terminosycondiciones" target= "_blank">enlace.</a></small>
        '''
    )

    formato_salida_path = f'FormatoSalida-{folio}.pdf'
    with open(formato_salida_path, 'rb') as f:
        dataPDF = f.read()
        f.close()
    encoded = base64.b64encode(dataPDF).decode()
    formatoSalidaPDF = Attachment()
    formatoSalidaPDF.file_content = FileContent(encoded)
    formatoSalidaPDF.file_type = FileType('application/pdf')
    formatoSalidaPDF.file_name = FileName(f'solicitud_{folio}.pdf')
    formatoSalidaPDF.disposition = Disposition('attachment')

    message.attachment = formatoSalidaPDF

    # if docHistopato != '':
    #     encoded = base64.b64encode(docHistopato).decode()
    #     histopatologico = Attachment()
    #     histopatologico.file_content = FileContent(encoded)
    #     histopatologico.file_type = FileType('application/pdf')
    #     histopatologico.file_name = FileName('histopatologico.pdf')
    #     histopatologico.disposition = Disposition('attachment')
    #     message.attachment = histopatologico

    # if docOtros != '':
    #     encoded = base64.b64encode(docOtros).decode()
    #     otro = Attachment()
    #     otro.file_content = FileContent(encoded)
    #     otro.file_type = FileType('application/pdf')
    #     otro.file_name = FileName('otro.pdf')
    #     otro.disposition = Disposition('attachment')
    #     message.attachment = otro

    return envioSendgrid(message)


def sendEmailFirma(folio, nomDoc, correoDoc):
    ''' Args: correoDoc, folio, nomDoc, docHistopato, docOtros '''

    message = Mail(
        from_email=(correo_base, 'Equipo de Diagnostique A Tiempo'),
        to_emails=[correo_base, correoDoc],
        subject=f'Solicitud #{folio} - Firma requerida',
        html_content=f'''
        <p>Estimad@ Dr@ {nomDoc},</p>
        <p>Su solicitud de estudio con folio #{folio} se encuentra disponible para ser firmada en el siguiente <a href="https://diagnostiqueatiempo.com/" target= "_blank">enlace</a>.</p>
        <p>Su firma es necesaria para atestar que la solicitud ha sido hecha por el médico solicitante e iniciar el proceso de la muestra proporcionada.</p>
        <p>Si necesita soporte adicional, puede contactarnos por las siguientes vías:</p>
        <ul>
        <li>Teléfono: +52 55 8880 7871</li>
        <li>Correo electrónico: respondiendo este correo electrónico o en contacto@diagnostiqueatiempo.com si su consulta es sobre otro tópico.</li>
        <li>Whatsapp: en el siguiente <a href="https://wa.me/525588807871">enlace</a></li>
        <br>
        <p>Atentamente,</p>
        <p>El equipo de Diagnostique A Tiempo</p>
        <br>
        <p>www.diagnostiqueatiempo.com</p>
        <small>Si no ha solicitado ningún estudio nuevo, favor de ponerse en contacto con el equipo.</small>
        <br>
        <small>De acuerdo a la Ley General de Protección de Datos en Posesión de Particulares, puede consultar nuestro Aviso de Privacidad y Términos y Condiciones del Servicio en el siguiente <a href="https://diagnostiqueatiempo.com/terminosycondiciones" target= "_blank">enlace.</a></small>
        '''
    )

    return envioSendgrid(message)


def sendEmailResListo(folio, nomDoc, correoDoc):
    ''' Args: correoDoc, folio, nomDoc, docHistopato, docOtros '''

    message = Mail(
        from_email=(correo_base, 'Equipo de Diagnostique A Tiempo'),
        to_emails=[correo_base, correoDoc],
        subject=f'Resultado #{folio} se encuentra disponible',
        html_content=f'''
        <p>Estimad@ Dr@ {nomDoc},</p>
        <p>Su resultado con folio #{folio} se encuentra disponible y puede consultarlo en <a href="https://diagnostiqueatiempo.com/" target= "_blank">www.diagnostiqueatiempo.com</a> ingresando su correo electrónico y contraseña.</p>
        <p>Si necesita soporte adicional, puede contactarnos por las siguientes vías:</p>
        <ul>
        <li>Teléfono: +52 55 8880 7871</li>
        <li>Correo electrónico: respondiendo este correo electrónico o en contacto@diagnostiqueatiempo.com si su consulta es sobre otro tópico.</li>
        <li>Whatsapp: en el siguiente <a href="https://wa.me/525588807871">enlace</a></li>
        <br>
        <p>Atentamente,</p>
        <p>El equipo de Diagnostique A Tiempo</p>
        <br>
        <p>www.diagnostiqueatiempo.com</p>
        <small>Si no ha solicitado ningún estudio nuevo, favor de ponerse en contacto con el equipo.</small>
        <br>
        <small>De acuerdo a la Ley General de Protección de Datos en Posesión de Particulares, puede consultar nuestro Aviso de Privacidad y Términos y Condiciones del Servicio en el siguiente <a href="https://diagnostiqueatiempo.com/terminosycondiciones" target= "_blank">enlace.</a></small>
        '''
    )

    return envioSendgrid(message)
