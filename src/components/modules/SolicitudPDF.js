import React from 'react';
import html2canvas from 'html2canvas';

import { formatoFechaHumano } from '../utils/Fecha';

const SolicitudPDF = ({ folio, nombrePaciente, apellidoPaciente, fechaNac, genero, categoriaElegida, estudioElegido, metodologiaEstudio, claveEstudio, nomDoc, cedula, telefono, email, costoEstudio, firmaPaciente, firmaMedico, skipFirmaPaciente }) => {

  window.onafterprint = () => {
    const printSection = document.getElementById('printSection');
    printSection.innerHTML = "";
  }

  const displaySignatures = () => {
    if ((firmaPaciente || skipFirmaPaciente) && firmaMedico) {
      return <>
         <tr>
            <td colSpan="16" className="spacer"></td>
         </tr>
        <tr>
          <td className="titulos-blancos" colSpan="16">Consentimiento</td>
        </tr>
        <tr>
          <td className="textos-justificados" colSpan="16">
            <p>
              Al firmar el presente Formato de Solicitud de Estudios declaro que he leído, acepto y entiendo los términos del AVISO DE PRIVACIDAD y CONSENTIMIENTO INFORMADO contenido al reverso de este documento.
            </p>
          </td>
        </tr>
        {
          skipFirmaPaciente ?
            <tr>
              <td className="textos-bottom" colSpan="8" rowSpan="2"><img src={firmaMedico} className="img-fluid d-block mx-auto" alt="falta firma médico" /> Nombre y firma del médico solicitante</td>
            </tr>
            :
            <tr>
              <td className="textos-bottom" colSpan="8" rowSpan="2"><img src={firmaPaciente} className="img-fluid d-block mx-auto" alt="falta firma paciente" />Nombre y firma del paciente o representante legal</td>
              <td className="textos-bottom" colSpan="8" rowSpan="2"><img src={firmaMedico} className="img-fluid d-block mx-auto" alt="falta firma médico" /> Nombre y firma del médico solicitante</td>
            </tr>
        }
      </>;
    }

    return <>
      <tr>
        <td className="titulos-blancos" colSpan="16">Consentimiento</td>
      </tr>
      <tr>
        <td className="textos-justificados" colSpan="16">
          <p>
            Al firmar el presente Formato de Solicitud de Estudios declaro que he leído, acepto y entiendo los términos del AVISO DE PRIVACIDAD y CONSENTIMIENTO INFORMADO contenido al reverso de este documento.
            </p>
        </td>
      </tr>
      {
        skipFirmaPaciente ?
          <tr>
            <td className="textos-bottom" colSpan="8" rowSpan="2"><img src="" className="img-fluid d-block mx-auto" alt="" /> Nombre y firma del médico solicitante</td>
          </tr>
          :
          <tr>
            <td className="textos-bottom" colSpan="8" rowSpan="2"><img src="" className="img-fluid d-block mx-auto" alt="" /> Nombre y firma del paciente o representante legal</td>
            <td className="textos-bottom" colSpan="8" rowSpan="2"><img src="" className="img-fluid d-block mx-auto" alt="" /> Nombre y firma del médico solicitante</td>
          </tr>
      }
    </>;
  };

  return (
    <div id="VisualizarPDF" className="modal fade">
      <div className="modal-dialog modal-xl">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">Previsualización</h5>
            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div id="printSection" style={{ position: 'fixed', left: 0, top: 0, maxWidth: '100%' }}>
          </div>
          <div className="modal-body">
            <div className="FSalida">
               <div className="datas Dfolio"></div>
               <div className="datas Dnombre"></div>
               <div className="datas DnacDia"></div>
               <div className="datas DnacMes"></div>
               <div className="datas DnacYear"></div>
               <div className="datas DgenMasc"></div>
               <div className="datas DgenFem"></div>
               <div className="datas DgenUndef"></div>
               <div className="datas DnomMed"></div>
               <div className="datas DmailMed"></div>
               <div className="datas DtelMed"></div>
               <div className="datas DinfMues1"></div>
               <div className="datas DinfMues2"></div>
               <div className="datas DinfMues3"></div>
               <div className="datas DinfMues4"></div>
               <div className="datas DinfMes"></div>
               <div className="datas DinfYear"></div>
               <div className="datas DinfTratPrev1"></div>
               <div className="datas DinfTratPrev2"></div>
               <div className="datas DinfTratPrev3"></div>
               <div className="datas DinfColHosp"></div>
               <div className="datas DinfColHosp2"></div>
               <div className="datas DinfColDir"></div>
               <div className="datas DinfColDir2"></div>
               <div className="datas DinfColRef"></div>
               <div className="datas DinfColRef2"></div>
               <div className="datas DlabEnvMuestra"></div>
               <div className="datas DlabEnvMuestra2"></div>
               <div className="datas DhoraEntrega1"></div>
               <div className="datas DhoraEntrega2"></div>
               <div className="datas DhoraEntrega3"></div>
               <div className="datas DhoraEntrega4"></div>
               <div className="datas DFirma"></div>
            </div>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="button" className="btn btn-primary"
              onClick={() => {
                html2canvas(document.getElementById("Datos"), { scrollX: 0, scrollY: -window.pageYOffset }).then(canvas => {
                  document.getElementById("printSection").appendChild(canvas);
                  window.print();
                });
              }}
            >
              Imprimir
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SolicitudPDF;
