import React from 'react';
import ApCi from './ApCi';
import ApiCiFindtrack from './ApCiFindtrack';

const ModalApCi = ({ skipFirmaPaciente }) => {

  const guardarModal = () => {
    window.hideModal("#ModalApCi");
  };

  return (
    <div id="ModalApCi" className="modal fade">
      <div className="modal-dialog modal-xl">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">Aviso de Provacidad y Consentimiento Informado</h5>
            <button type="button" className="close" onClick={guardarModal} aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div className="container-fluid">
              {
                skipFirmaPaciente ?
                  <ApiCiFindtrack />
                  : <ApCi />
              }
            </div>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-secondary" onClick={guardarModal}>Cerrar</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalApCi;