export { default as DATLogo } from './DAT-logo.svg';
export { default as FondoProgBay } from './imgProgramBayer.svg';
export { default as LogoBayColor } from './Logo-Bayer2-original.png';
export { default as MiniLogoBlanco } from './Logo-Bayer.svg';