from flask import send_file
from flask_restful import Resource, request

from pdf import generatePDF, generateBayer
from schemas.pdf import PdfSchema, PDFBayerSchema

pdf_schema = PdfSchema()
pdf_bayer_schema = PDFBayerSchema()


class PDFGen(Resource):
    ''' Generar PDF y guardarlo en servidor '''

    def post(self):
        sol_json = request.get_json()
        sol_data = pdf_schema.load(sol_json)

        if sol_data['folio']:
            return send_file(f'FormatoSalida-{sol_data["folio"]}.pdf', generatePDF(**sol_data))

        # return send_file('FormatoSalida-.pdf', generatePDF())
        return {'message': 'Faltan datos. Si persiste este problema, contacte al equipo de soporte.'}, 400


class PDFDisplay(Resource):
    ''' Se debe tener el PDF previamente. Aquí sólo es mostrarlo en cualquier momento. '''

    def get(self, folio):
        try:
            return send_file(f'FormatoSalida-{folio}.pdf')
        except Exception:
            return {'error': 'Archivo no encontrado. Contacte al equipo de soporte.'}, 404


class PDFGenBayer(Resource):
    ''''''
    def post(self):
        sol_json = request.get_json()
        sol_data = pdf_bayer_schema.load(sol_json)

        if sol_data['folio']:
            return send_file(f'FormatoBayer-{sol_data["folio"]}.pdf', generateBayer(**sol_data))
        
        return {'message': 'Faltan datos. Si persiste este problema, contacte al equipo de soporte.'}, 400