export const exportXMLFile = arrOfObj => {
  const fileName = `misEstudiosXML-fecha-${new Date().getFullYear()}-${new Date().getMonth()}-${new Date().getDate()}`;

  // const jsonParsed = JSON.parse(jsonString);
  // const xmlStr = new window.X2JS().json2xml_str(jsonParsed);
  const a = document.createElement('a');
  a.download = fileName;
  a.href = URL.createObjectURL(new File([xmlStr(arrOfObj)], fileName, { type: 'text/xml' }));
  a.click();
};

const xmlStr = arrOfObj => {
  const contenido = arrOfObj.reduce((result, el) => {
    return result + `<estudio>
      <folio>${el.folio}</folio>
      <estudio_solicitado>${el.estudio}</estudio_solicitado>
      <nombreDoctor>${el.nomDoc}</nombreDoctor>
      <nombrePaciente>${el.nombrePaciente}</nombrePaciente>
      <apellidoPaciente>${el.apellidoPaciente}</apellidoPaciente>
      <fechaNacimientoPaciente>${el.fechaNac}</fechaNacimientoPaciente>
      <generoPaciente>${el.genero}</generoPaciente>
      <emailPaciente>${el.email}</emailPaciente>
      <telefonoPaciente>${el.telefono}</telefonoPaciente>
      <celularPaciente>${el.celular}</celularPaciente>
      <denominacionSocial>${el.denominacionSocial}</denominacionSocial>
      <RFC>${el.RFC}</RFC>
      <emailFactura>${el.emailFactura}</emailFactura>
      <firmaEnImpresion>${el.file}</firmaEnImpresion>
      <reporteHistopatologico>${el.reporteHistopatologico}</reporteHistopatologico>
      <otroArchivo>${el.otroArchivo}</otroArchivo>
      <pagoPaciente>${el.pago}</pagoPaciente>
      <costoEstudio>${el.costo}</costoEstudio>
      <fechaEnvioSolicitud>${el.fecha_solcitada}</fechaEnvioSolicitud>
      <etapaActualSolicitud>${el.etapa}</etapaActualSolicitud>
      <firmaPaciente>${el.firmaPaciente}</firmaPaciente>
      <firmaMedico>${el.firmaMedico}</firmaMedico>
      <tipoDeMuestra>${el.muestraBloque}</tipoDeMuestra>
      <identificadorDeLaMuestra>${el.numeroMuestraBloque}</identificadorDeLaMuestra>
      <fechaTomaDeMuestra>${el.fechaMuestra}</fechaTomaDeMuestra>
      <diagnosticoDeLaMuestra>${el.diagnostico}</diagnosticoDeLaMuestra>
      <tratamientosPrevios>${el.tratamientosPrevios}</tratamientosPrevios>
      <direccion_calleColecta>${el.direccion_calle}</direccion_calleColecta>
      <direccion_coloniaColecta>${el.direccion_colonia}</direccion_coloniaColecta>
      <direccion_cpColecta>${el.direccion_cp}</direccion_cpColecta>
      <direccion_estadoColecta>${el.direccion_estado}</direccion_estadoColecta>
      <direccion_municipioColecta>${el.direccion_municipio}</direccion_municipioColecta>
      <direccion_hospitalColecta>${el.direccion_hospital}</direccion_hospitalColecta>
      <direccion_referenciasColecta>${el.direccion_referencias}</direccion_referenciasColecta>
      <horarioRecoleccionMuestra>${el.horarioRecoger}</horarioRecoleccionMuestra>
      <laboratorioSeleccionadoAnalisisMuestra>${el.laboratorioSeleccionado}</laboratorioSeleccionadoAnalisisMuestra>
      <empresaPharma>${el.empresaPharma}</empresaPharma>
      <colecta_programada>${el.colecta_programada}</colecta_programada>
      <fechaColectaRealizada>${el.colecta_realizada}</fechaColectaRealizada>
      <fechaEntregaEnLab>${el.entregada_lab}</fechaEntregaEnLab>
      <estatusResuladoEmitido>${el.resultado_emitido_estatus}</estatusResuladoEmitido>
      <fechaResultadoEmitido>${el.resultado_emitido_fecha}</fechaResultadoEmitido>
      <comentariosResultadoEmitido>${el.resultado_emitido_comentarios}</comentariosResultadoEmitido>
      <archivoResultadoEmitido>${el.resultado_emitido_filename}</archivoResultadoEmitido>
      <fechaRegresoDeLaColecta>${el.regreso_muestra}</fechaRegresoDeLaColecta>
    </estudio>\n`
  }, '');

  return `<estudios>${contenido}</estudios>`;
};