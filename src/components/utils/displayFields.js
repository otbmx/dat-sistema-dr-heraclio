export const ToggleCampos = (idEdicion, ocultarEditar) => {
  /**
   * @const {*} campos - HTML Collection que poseen la calse desabilitadoVAR, donde VAR es un número.
   */
  const campos = Array.from(document.getElementsByClassName(`deshabilitado${idEdicion}`));
  
  campos.forEach(campo => campo.disabled = ocultarEditar);
}