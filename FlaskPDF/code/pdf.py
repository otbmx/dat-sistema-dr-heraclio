'''
Documentación: https://www.reportlab.com/docs/reportlab-userguide.pdf
Ejemplo: https://www.youtube.com/watch?v=ZDR7-iSuwkQ
'''

from reportlab.pdfgen.canvas import Canvas
# from reportlab.lib import utils
from reportlab.lib.units import mm
# from reportlab.platypus import Image
from reportlab.lib.utils import ImageReader
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.platypus import BaseDocTemplate, PageTemplate, Frame, Paragraph, Image
from reportlab.lib.enums import TA_CENTER
from reportlab.lib import colors


# def get_image(path, width=210*mm):
#     # img = utils.ImageReader(path)
#     # iw, ih = img.getSize()
#     # aspect = ih / float(iw)
#     # return (width, (width * aspect))
#     return (width, 279.4*mm)


def generatePDF(folio, nombrePaciente, apellidoPaciente, nacDia, nacMes, nacAnio, genero,
categoriaElegida, estudioElegido, metodologiaEstudio, claveEstudio,
nomDoc, cedula, email, telefono,
primarioSelect, muestraBloque, numeroMuestraBloque, muestraLaminillas, numeroMuestraLaminillas, muestraMes, muestraAnio, diagnostico, labRadio,
inputNombreHospital, inputCalleNumero, inputCodigoPostal, inputColinia, inputAlcaldiaMun, inputEstado, inputReferencias, horarioRecoger,
tratamientosPrevios='', costoEstudio=0, firmaPaciente='', firmaMedico='', docHistopato='', docOtros='', metastasicoSelect=''):

    filename = f'FormatoSalida-{folio}.pdf'
    docTtitle = 'Formato de Salida'
    imagen = './FS_DIGITAL.jpg'
    imagen2 = './FS_DIGITAL2.jpg'
    pdf = Canvas(filename)
    pdf.setPageSize((125*mm, 280*mm))

    pdf.setTitle(docTtitle)
    # drawInlineImage(imagen, x, y, w, h, preserveAspectRatio=True)
    # w, h = get_image(imagen, 175*mm)
    # pdf.drawImage(imagen, 50, 25, 175*mm, 279.4*mm)
    pdf.drawImage(imagen, 0, 0, 125*mm, 280*mm)
    pdf.setFontSize(7)
    pdf.drawString(255, 747, folio)
    pdf.drawString(25, 685, f'{nombrePaciente}')
    pdf.drawString(145, 685, apellidoPaciente)
    pdf.drawString(47, 627, f'{nacDia}')
    pdf.drawString(85, 627, f'{nacMes}')
    pdf.drawString(117, 627, f'{nacAnio}')

    if genero == "Femenino":
        pdf.drawString(237, 645, 'X')  # fem
    elif genero == "Masculino":
        pdf.drawString(237, 628, 'X')  # mas
    else:
        pdf.drawString(237, 612, 'X')  # no det

    pdf.drawString(75, 553, nomDoc)
    pdf.drawString(75, 536, email)
    pdf.drawString(75, 516, telefono)
    if primarioSelect == 'primario':
        pdf.drawString(22, 452, 'X')  # tumor primario
    else:
        pdf.drawString(22, 425, 'X')  # metastásico

    if metastasicoSelect != '':
        pdf.drawString(22, 405, metastasicoSelect)  # metastásico
    
    if muestraLaminillas:
        pdf.drawString(127, 452, 'X')  # lam
    if muestraBloque:
        pdf.drawString(127, 425, 'X')  # bloque
    
    corte = 25
    if len(numeroMuestraBloque) > corte:
        refParte1 = numeroMuestraBloque[:corte]
        refParte2 = numeroMuestraBloque[corte:]
        pdf.drawString(120, 390, refParte1)
        pdf.drawString(120, 375, refParte2)

        pdf.drawString(120, 360, numeroMuestraLaminillas)
    else:
        pdf.drawString(120, 390, numeroMuestraBloque)
        pdf.drawString(120, 375, numeroMuestraLaminillas)

    pdf.drawString(242, 440, f'{muestraMes}')
    pdf.drawString(282, 440, f'{muestraAnio}')

    if len(diagnostico) > corte:
        refParte1 = diagnostico[:corte]
        refParte2 = diagnostico[corte:]
        pdf.drawString(220, 390, refParte1)
        pdf.drawString(220, 375, refParte2)
    else:
        pdf.drawString(220, 390, diagnostico)

    if tratamientosPrevios != '':
        if tratamientosPrevios == "Sin tratamiento previo":
            pdf.drawString(117, 343, 'X')  # sin tratamiento
        elif tratamientosPrevios == "2L":
            pdf.drawString(213, 343, 'X')  # 2l
        else:
            pdf.drawString(246, 343, 'X')  # 3l
    
    pdf.drawString(60, 280, inputNombreHospital)
    pdf.drawString(65, 244, f'{inputCalleNumero}, CP {inputCodigoPostal}')
    pdf.drawString(25, 227, f'Colonia {inputColinia}, {inputAlcaldiaMun}, {inputEstado}')
    
    # Definición referencias en dos líneas
    corte = 65
    if len(inputReferencias) > corte:
        # Ejemplo: edificio de oncología, piso 2, servicio oncogenetica, # 53
        refParte1 = inputReferencias[:corte]
        refParte2 = inputReferencias[corte:]
        pdf.drawString(25, 192, f'{refParte1}')
        pdf.drawString(25, 175, refParte2)
    else:
        pdf.drawString(25, 192, inputReferencias)
      
    pdf.drawString(25, 140, labRadio)
    if horarioRecoger == "9 a 12 hrs":
        pdf.drawString(238, 255, 'X')  # 9 a 12
    elif horarioRecoger == "11 a 14 hrs":
        pdf.drawString(238, 232, 'X')  # 11 a 14
    elif horarioRecoger == "15 a 18 hrs":
        pdf.drawString(238, 209, 'X')  # 15 a 18
    else:
        pdf.drawString(238, 186, 'X')  # 9 a 18

    if firmaMedico != '':
        pdf.drawImage(firmaMedico, 55, 6, 30*mm, preserveAspectRatio=True, mask='auto')

    # Salto de página
    pdf.showPage()
    # w, h = get_image(imagen2, 175*mm)
    # pdf.drawImage(imagen2, 50, 25, 175*mm, 279.4*mm)
    pdf.drawImage(imagen2, 0, 0, 125*mm, 280*mm)

    pdf.save()


def draw_static(canvas, doc):
    # Save the current settings
    canvas.saveState()

    # Draw the static stuff
    canvas.setFont('Times-Bold', 12, leading=None)
    # canvas.drawCentredString(200, 200, "Static element")

    # Restore setting to before function call
    canvas.restoreState()


def generateBayer(folio, nomDoc, firmaMedico):
    '''Se necesita folio y firma para enviar este doc'''

    # filename = f'FormatoBayer-{folio}.pdf'
    # docTtitle = 'Aviso de Privacidad Bayer'
    # pdf = Canvas(filename)
    # pdf.setPageSize((215*mm, 279*mm))

    # pdf.setTitle(docTtitle)
    # pdf.setFontSize(14)
    # pdf.drawString(225, 770, docTtitle)
    # pdf.setFontSize(9)

    # pdf.textLines(5, 747, '''Bayer de México S.A. de C.V. (en lo sucesivo “Bayer”) pone a su disposición el presente Aviso de Privacidad con la finalidad de darle a conocer el tratamiento que le dará a sus datos personales de conformidad con las disposiciones de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares y su Reglamento (en lo sucesivo “la Ley”).
    # ''')

    # pdf.drawImage(firmaMedico, 155, 6, 30*mm, preserveAspectRatio=True, mask='auto')

    # pdf.save()

    # Set up a basic template
    doc = BaseDocTemplate(f'FormatoBayer-{folio}.pdf')

    # Create a Frame for the Flowables (Paragraphs and such)
    frame = Frame(doc.leftMargin, doc.bottomMargin, doc.width, doc.height, id='normal')

    # Add the Frame to the template and tell the template to call draw_static for each page
    template = PageTemplate(id='FormatoBayer', frames=[frame], onPage=draw_static)

    # Add the template to the doc
    doc.addPageTemplates([template])

    # All the default stuff for generating a document
    styles = getSampleStyleSheet()
    # styles.alignment = TA_CENTER
    story = []

    bodyStyleTitle = ParagraphStyle('Body', fontName='Times-Bold', fontSize=12, leading=28, spaceBefore=3)
    bodyStyle = ParagraphStyle('Body', fontName='Times-Bold', fontSize=10, leading=28, spaceBefore=0)

    P = Paragraph('Aviso de Privacidad de Bayer de México S.A. de C.V.', bodyStyleTitle)
    story.append(P)
    P = Paragraph('Bayer de México S.A. de C.V. (en lo sucesivo “Bayer”) pone a su disposición el presente Aviso de Privacidad con la finalidad de darle a conocer el tratamiento que le dará a sus datos personales de conformidad con las disposiciones de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares y su Reglamento (en lo sucesivo “la Ley”).', bodyStyle)
    story.append(P)

    P = Paragraph('Identidad y domicilio del responsable del tratamiento de sus datos personales.', bodyStyleTitle)
    story.append(P)
    P = Paragraph('Bayer, con  domicilio en  Boulevard  Miguel  de Cervantes Saavedra  259, Col. Granada, Delegación Miguel Hidalgo, Ciudad de México, C.P. 11520 es el responsable en términos de la Ley.', bodyStyle)
    story.append(P)

    P = Paragraph('Datos Personales.', bodyStyleTitle)
    story.append(P)
    P = Paragraph('Los datos personales que serán sujetos a tratamiento por parte de Bayer pertenecen a las siguientes categorías:', bodyStyle)
    story.append(P)
    P = Paragraph('• Datos de identificación y contacto: Nombre, domicilio, teléfono, correo electrónico, CURP, fecha de nacimiento, edad, nacionalidad, fotografía y firma.', bodyStyle)
    story.append(P)
    P = Paragraph('• Datos laborales: Puesto  o cargo, domicilio laboral, correo  electrónico, teléfono y fax del trabajo, institución de salud a la que pertenece.', bodyStyle)
    story.append(P)
    P = Paragraph('• Datos académicos: Trayectoria educativa, título, número de cédula profesional, especialidad, tipos de práctica, certificados de estudios, información sobre sus intereses científicos y/o médicos, actividad profesional como investigador.', bodyStyle)
    story.append(P)
    P = Paragraph('• Datos financieros o patrimoniales. RFC, CLABE y Domicilio de facturación.', bodyStyle)
    story.append(P)
    P = Paragraph('• Datos de sus hábitos de consumo.', bodyStyle)
    story.append(P)

    P = Paragraph('Transferencias de datos personales', bodyStyleTitle)
    story.append(P)
    P = Paragraph('Bayer únicamente transferirá sus datos personales a terceros, nacionales o extranjeros, que estén relacionados jurídica o comercialmente con Bayer para cumplir con las finalidades descritas en el presente Aviso de Privacidad. Asimismo, Bayer podrá transferir sin su consentimiento sus datos personales en los casos previstos y autorizados por la Ley.', bodyStyle)
    story.append(P)
    P = Paragraph('En el caso que Bayer venda una unidad comercial a otra compañía y sus datos personales sean usados por dicha unidad comercial, los mismos podrán transferirse al comprador junto con el negocio a fin de que el comprador pueda utilizarlos de la misma manera en que Bayer los utilizaba.', bodyStyle)
    story.append(P)
    P = Paragraph('El Titular otorga su autorización para que Bayer transfiera sus Datos Personales a terceros, ya sean mexicanos o extranjeros sin que para ello se requiera recabar nuevamente su consentimiento, cuando la transferencia sea efectuada a sociedades subsidiarias o afiliadas bajo el control común del Responsable o a una sociedad matriz o a cualquier sociedad del mismo grupo de Bayer que opere bajo los mismos procesos y políticas internas.', bodyStyle)
    story.append(P)

    P = Paragraph('Medidas de seguridad', bodyStyleTitle)
    story.append(P)
    P = Paragraph('Bayer ha implementado y mantiene las medidas de seguridad, técnicas, administrativas y físicas, necesarias para proteger sus datos personales y evitar su daño, pérdida, alteración, destrucción o el uso, acceso o tratamiento no autorizado.', bodyStyle)
    story.append(P)

    P = Paragraph('Derechos del titular de los datos personales', bodyStyleTitle)
    story.append(P)
    P = Paragraph('Usted o su representante legal debidamente acreditado, podrán ejercer los derechos de Acceso, Rectificación, Cancelación y Oposición (derechos “ARCO”) respecto de sus datos personales. Asimismo, usted puede limitar el uso o divulgación de sus datos personales y revocar el consentimiento que haya otorgado para el tratamiento de sus datos personales, siempre y cuando el tratamiento no resulte necesario o derive de una relación jurídica.', bodyStyle)
    story.append(P)
    P = Paragraph('El ejercicio de los derechos previstos en la Ley se podrá llevar a cabo a través del envío de su solicitud en los términos establecidos por la Ley y su Reglamento, o el envío del formato sugerido que encontrará disponible en el sitio de internet www.bayer.mx, al correo electrónico data.protectionmx@bayer.com o en nuestras instalaciones ubicadas en Boulevard Miguel de Cervantes Saavedra 259, Col. Granada. México, Ciudad de México, C.P. 11520.', bodyStyle)
    story.append(P)

    P = Paragraph('Modificaciones al Aviso de Privacidad.', bodyStyleTitle)
    story.append(P)
    P = Paragraph('Bayer se reserva el derecho de enmendar o modificar el presente Aviso de Privacidad según estime conveniente, por ejemplo, para cumplir con cambios a la legislación o cumplir con disposiciones internas de Bayer.', bodyStyle)
    story.append(P)
    P = Paragraph('De requerir notificarle de los cambios en el tratamiento de datos personales o recabar nuevamente su consentimiento, Bayer pondrá a su disposición el Aviso de Privacidad actualizado al correo electrónico que nos haya proporcionado o en el sitio web www.bayer.mx.', bodyStyle)
    story.append(P)

    P = Paragraph('Contacto', bodyStyleTitle)
    story.append(P)
    P = Paragraph('Podrá dirigir preguntas o comentarios respecto al presente Aviso de Privacidad y otras cuestiones de protección de datos personales al Departamento de Protección de Datos Personales a través de los siguientes canales: correo electrónico: data.protectionmx@bayer.com; teléfono: (01 55) 5713 2288; o directamente en nuestras oficinas en: Boulevard Miguel de Cervantes Saavedra 259, Col. Granada, Delegación Miguel Hidalgo, Ciudad de México, C.P. 11520', bodyStyle)
    story.append(P)

    P = Paragraph('Fecha de última actualización: mayo 2019', bodyStyleTitle)
    story.append(P)

    img = Image(firmaMedico)
    img.drawWidth = 30*mm
    img.drawHeight = 30*mm
    story.append(img)

    styles.add(ParagraphStyle(name='Normal_CENTER',
                          parent=styles['Normal'],
                          wordWrap='LTR',
                          alignment=TA_CENTER,
                          fontSize=12,
                          leading=13,
                          textColor=colors.black,
                          borderPadding=0,
                          leftIndent=0,
                          rightIndent=0,
                          spaceAfter=0,
                          spaceBefore=0,
                          splitLongWords=True,
                          spaceShrinkage=0.05,
                          ))

    P = Paragraph(f'Firma de {nomDoc}', styles['Normal_CENTER'])
    story.append(P)


    doc.build(story)